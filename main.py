import math
import random

if __name__ == '__main__':
    print('Hello World')

a = 10
b = '3'

if a > int(b):
    print(f"{a} is superior to {b}")
    print(a + int(b))  # Je suis un commentaire
else:
    print(f'{a} is inferior to {b}')

print('coucou')
print('a')
print("a")  # Python est sensible à la casse

"""
je suis un commentaire
"""

PascalCase = ''
camelCase = ''
snake_case = ''

PascalCase, camelCase, snake_case = 'Hello ', 'World ', "!"
print(PascalCase + camelCase + snake_case)

legumes = ['patates', 'tomates', 'pasteques']
legumes.pop()
print(legumes)
legumes.append('oui')
legumes.insert(0, "fraises")
print(legumes)

global maVariableGlobal


def function(variable):
    print(variable)


function('Hello')
# import random
print(random.randrange(2, 10))

string = """
tres long string 
"""
print(string)

for i in string:
    print(i)

string = "Hello world     "
print(string.split(" "))

if "dude" not in string:
    print("dude is not include in string")

print(string[2:])
print(string[2:4])
print(string[-2:])
print(string.upper())
print(string.lower())
print(string.strip())

# Exercice 1

prenom = "Pierre"
age = 20
majeur = True
compteur_en_banque = 20135.384
amis = ["Marie", "Julien", "Adrien"]
parents = ('Marc', 'Caroline')

# Exercice 2

site_web = "voila"
print(site_web)

# Exercice 3

nombre = 15
print(f"Le nombre est {nombre}")  # faire une 'fstring' et mettre la variable nombre en brackets

# Exercice 5

a = 2
b = 6
c = 3

print(f'{a}+{b}+{c}')  # utiliser fstring
print(a, b, c, sep='+')  # separator in print Python3

# Exerice 6

prenom2 = 0
print(type(prenom2))
if isinstance(prenom2, str):  # type(prenom2) is str
    print('La variable est une chaine de caractere')
else:
    print("ce n'est pas une chaine de caractere")

# Exercice 7

phrase = "Bonjour les amis"
nouvelle_phrase = phrase.replace("Bonjour", "Salut")
print(nouvelle_phrase)

print(bool(10))
print(bool(False))


def maFonction():
    return True


maFonction()
phrase_test = "Bonjour tout le monde"
occurence = ''
nombreOccurence = ''
replaceOccurence = ''
"""
while not nombreOccurence.isdecimal():
    print(f"Quelle occurence voulez enlever dans la phrase {phrase_test}")
    occurence = input()

    if phrase_test.find(occurence) is not -1:
        print(f"Combien voulez vous enlever d'occurences {occurence} dans la phrase\n {phrase_test}\n")
        nombreOccurence = input()
        print(f"Que voulez vous mettre à la place de l'occurence {occurence} dans votre phrase\n {phrase_test}")
        replaceOccurence = input()

    else:
        print(f"Attention l'occurence {occurence} n'existe pas dans la phrase\n {phrase_test}\n")

nouvelle_phrase = phrase_test.replace(occurence, replaceOccurence, int(nombreOccurence))
print(nouvelle_phrase)
"""
liste4 = ['pomme', 'poire', 'cerise']
liste4.insert(0, 'framboise')
print(liste4)
tuple1 = (1, 2, 3)
liste4.extend(tuple1)
print(liste4)
liste4.pop()
liste4.clear()  # remove all element in collection object
del liste4  # destroy collection object

liste12 = [1, 2, 3, 5, 6]
for i in range(len(liste12)):
    print(liste12[i])

# Exercice

liste2 = "pomme,cerise,abricot"
x = sorted(liste2.split(","))
message = ','.join(x)
print(message)


def calculeVolume():  # Attention pour le moment la fonction ne marche qu'avec des int
    rayon = ''
    while not rayon.isdecimal():
        print(f'Veuillez rentrer un rayon pour calculer son volume')
        rayon = input()
        if rayon.isdecimal():
            volume = ((4 * math.pi) * math.pow(float(rayon), 3)) / 3
            print(round(volume, 2))
        else:
            print("Attention votre rayon n'est pas un nombre")


# calculeVolume()

# Exercice

listeExo = []
listeExo.clear()
for i in range(10, 101):
    listeExo.append(i)
print(listeExo)

# Exercice
listeExo2 = []
listeExo2.clear()
for i in range(1, 201):
    if i % 2 == 0:
        listeExo2.append(i)
print(listeExo2)


# depuis Python 3 range ne retourne pas une liste mais un objet de type range

def generator():
    valeur_obtenu = 0
    moyenne = 0
    nombre_de_lancer = 15
    for _ in range(nombre_de_lancer + 1):
        valeur_lance = random.randrange(1, 7)
        valeur_obtenu += valeur_lance
        moyenne = round(valeur_obtenu / nombre_de_lancer, 2)
    print(moyenne)


generator()

# Exercice

chaine = 'Ala je suis la ?'

print(chaine.isspace())

def CountOccur(occur):
    count = 0
    if chaine.find(occur.upper()) != -1:
        count += chaine.count(occur.upper())
    count += chaine.count(occur)
    return count


CountOccur('i')

dictionnary = {}
key = 0
num = 0
for i in chaine:
    if not i.isspace():
        dictionnary[i] = CountOccur(i)

for i in dictionnary:
    if dictionnary[i] > num:
        num = dictionnary[i]
        key = i

print(f'la lettre la plus frequente dans la phrase est {key}. Il y en a {num}')

# Exercice

listeEx = [1,2,3,4,5,6,7,8]
listeEx2 = []
for i in range(listeEx[0],listeEx[len(listeEx)-1],2):
    listeEx2.append(i)
print(listeEx2)
print(listeEx[::2]) # ::2 slice step 2